// Load env values
const prefix = process.env.PREFIX;
const googleSpreadsheetId = process.env.GOOGLE_SHEET_ID;

//Load polling options
const { pollOptionsList } = require('../customization/pollOptions.json');

// at the top of your file
const Discord = require('discord.js');

const { GoogleSpreadsheet } = require('google-spreadsheet');

// Appends new row for user response on Google Sheets
const appendResponse = async (user, responseItem) => {
  // spreadsheet key is the long id in the sheets URL
  const doc = new GoogleSpreadsheet(googleSpreadsheetId);

  await doc.useServiceAccountAuth(require('../googleCreds.json'));
  await doc.loadInfo(); // loads document properties and worksheets

  //use first sheet
  const sheet = doc.sheetsByIndex[0];

  //Log response to console for pm2 and debug.
  console.log(
    `${user.username}#${user.discriminator} responded: ${responseItem.value}`
  );

  let date = new Date();

  // write to Google Sheets
  await sheet.addRow([
    user.username,
    user.username + '#' + user.discriminator,
    user.id,
    responseItem.value,
    date.toLocaleString('en-US', {
      timeZone: 'America/New_York',
      timeZoneName: 'short',
    }),
  ]);

  return;
};

module.exports = {
  name: 'poll', // command name
  execute(member) {
    console.log(
      `Poll initiated for ${member.username}#${member.discriminator}`
    );

    const data = [];

    // build list of emojis from poll options
    const reactions = pollOptionsList.map((role) => role.emoji);
    // console.log(reactions);

    // filter reactions to only those of the user
    // Discord
    const filter = (reaction, user) => {
      let validEmoji = false;

      if (reaction._emoji.id) {
        validEmoji = reactions.includes(
          `<:${reaction._emoji.name}:${reaction._emoji.id}>`
        );
      } else {
        validEmoji = reactions.includes(`${reaction._emoji.name}`);
      }

      return user.id === member.id && validEmoji;
    };

    // Format lines for the embed by iterating through pollOptionsList
    lines = pollOptionsList.map((option) => {
      return `${option.emoji}  ${option.label}\n\n`;
    });

    // Build embed to send to user.
    const embed = new Discord.MessageEmbed()
      .setColor('#58b560')
      .setTitle('Welcome to the Sia Community Server!')
      .setDescription('Where did you first hear about Sia/Skynet?')
      .setThumbnail(
        'https://siasky.net/EADRX8pgGnVWLKl-AyB80pBhcQSZUVycO6g-ySuIXv-MLw'
      )
      .addField('\u200b', lines.join(''));

    member.send(embed);

    data.push('Please select a reaction below to let us know!');

    // DM asking for channel to be added to
    member.send(data).then(async (m) => {
      // create a list of functions, each returns promise and is an emoji reaction
      const reactionActions = reactions.map((reaction) => m.react(reaction));

      // Different method than in chan.js, here all reactions in list above, when resolved
      // will get wrapped in promise and succeed or fail.
      // Also, ordering is not guaraneed since they're done async.

      Promise.all(reactionActions).catch(() =>
        console.error('One of the emojis failed to react.')
      );

      // Reaction Collector. Max 1 reaction, 50 sec timeout
      m.awaitReactions(filter, { max: 1, time: 50000, errors: ['time'] })
        .then((collected) => {
          const itemSelected = collected.keys().next().value;

          if (!!itemSelected) {
            let itemName = pollOptionsList.filter((i) =>
              i.emoji.includes(itemSelected)
            );

            if (itemName[0]) {
              itemName = itemName[0];
            } else {
              member.send(`Something went wrong.`);
            }

            appendResponse(member, itemName);

            member.send(`Thanks for responding. Enjoy the server!`);
          } else {
            member.send(`Something went wrong.`);
          }
        })
        .catch((err) => {
          member.send(
            `I didn't get a response or something went wrong. Type \`${prefix}${this.name}\` to try again.`
          );
        });
    });

    return;
  },
};
