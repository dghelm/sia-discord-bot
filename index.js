// Main file for Sia Discord Bot

// bring in process.env variables depending on env
require('dotenv-flow').config();

const fs = require('fs');
const Discord = require('discord.js');
const client = new Discord.Client({ partials: ['MESSAGE', 'REACTION'] });
client.commands = new Discord.Collection();

const Poll = require('./actions/poll');
const Welcome = require('./actions/welcome');

// Basic antispam settings from discord-anti-spam npm module
// const AntiSpam = require('discord-anti-spam');

// const antiSpam = new AntiSpam({
//   warnThreshold: 3, // Amount of messages sent in a row that will cause a warning.
//   kickThreshold: 7, // Amount of messages sent in a row that will cause a kick.
//   banThreshold: 7, // Amount of messages sent in a row that will cause a ban.
//   muteThreshold: 5, // Amount of messages sent in a row that will cause a mute.
//   maxInterval: 2000, // Amount of time (in milliseconds) in which messages are considered spam.
//   warnMessage: '{@user}, Please stop spamming.', // Message that will be sent in chat upon warning a user.
//   kickMessage: '**{user_tag}** has been kicked for spamming.', // Message that will be sent in chat upon kicking a user.
//   banMessage: '**{user_tag}** has been banned for spamming.', // Message that will be sent in chat upon banning a user.
//   muteMessage: '**{user_tag}** has been muted for spamming.', // Message that will be sent in chat upon muting a user.
//   maxDuplicatesWarning: 7, // Amount of duplicate messages that trigger a warning.
//   maxDuplicatesKick: 10, // Amount of duplicate messages that trigger a warning.
//   maxDuplicatesBan: 12, // Amount of duplicate messages that trigger a warning.
//   maxDuplicatesMute: 9, // Amount of duplicate messages that trigger a warning.
//   // Discord permission flags: https://discord.js.org/#/docs/main/master/class/Permissions?scrollTo=s-FLAGS
//   exemptPermissions: ['ADMINISTRATOR'], // Bypass users with any of these permissions(These are not roles so use the flags from link above).
//   ignoreBots: true, // Ignore bot messages.
//   verbose: true, // Extended Logs from module.
//   ignoredUsers: [], // Array of User IDs that get ignored.
//   // And many more options... See the documentation.
// });

// identifies all the commands in ./commands and sets on client

const commandFiles = fs
  .readdirSync('./commands')
  .filter((file) => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  // set a new item in the Collection
  // with the key as the command name and the value as the exported module
  client.commands.set(command.name, command);
}

// set vars from imported process env vars
const prefix = process.env.PREFIX;
const token = process.env.TOKEN;
const guildId = process.env.GUILD_ID;
const welcomeMessageId = process.env.WELCOME_MESSAGE_ID;

const cooldowns = new Discord.Collection();

client.once('ready', () => {
  console.log('Ready!');
});

client.on('guildMemberAdd', (member) => {
  if (process.env.BOT === 'dev') {
    if (!member.user.bot && member.guild.id == guildId) {
      Poll.execute(member.user);
    }
  }
});

client.login(token);

client.on('messageReactionAdd', async (reaction, user) => {
  // When we receive a reaction we check if the reaction is partial or not
  if (reaction.partial) {
    // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
    try {
      await reaction.fetch();
    } catch (error) {
      console.error('Something went wrong when fetching the message: ', error);
      // Return as `reaction.message.author` may be undefined/null
      return;
    }
  }
  // Now the message has been cached and is fully available

  if (reaction.message.id == welcomeMessageId) {
    return Welcome.appendReaction(reaction, user);
  }
});

client.on('message', (message) => {
  // console.log(message.channel);

  // message needs to use command prefix and not be from bot
  if (!message.content.startsWith(prefix) || message.author.bot) return;
  // return antiSpam.message(message);

  // get command name and arguments from message
  const args = message.content.slice(prefix.length).trim().split(/ +/);
  const commandName = args.shift().toLowerCase();

  // if there isn't a command name or alias matching message command, do nothing.
  // pull the correct command and its attributes

  const command =
    client.commands.get(commandName) ||
    client.commands.find(
      (cmd) => cmd.aliases && cmd.aliases.includes(commandName)
    );

  if (!command) return;

  // check if guildOnly (not for DMs)
  if (command.guildOnly && message.channel.type === 'dm') {
    return message.reply("I can't execute that command inside DMs!");
  }

  if (command.devOnly && process.env.BOT !== 'dev') {
    return;
  }

  // check if args needed
  if (command.args && !args.length) {
    let reply = `You didn't provide any arguments, ${message.author}!`;

    if (command.usage) {
      reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
    }

    return message.channel.send(reply);
  }

  // start code for enforcing cooldowns

  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 3) * 1000;

  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;
      return message.reply(
        `please wait ${timeLeft.toFixed(
          1
        )} more second(s) before reusing the \`${command.name}\` command.`
      );
    }
  }

  //end code for enforcing cooldowns

  // code for calling command execute method

  try {
    command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply('There was an error trying to execute that command!');
  }
});
