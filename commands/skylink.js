const skynet = require('skynet-js');
const Discord = require('discord.js');
const axios = require('axios');
const cheerio = require('cheerio');
const { URL } = require('url');

module.exports = {
  name: 'skylink',
  aliases: ['sl'], // aliases for command name
  args: true,
  usage: '<skylink>', //text helping provide usage info
  description: 'Returns info about a skylink.',
  devOnly: true, // only bot with 'dev' value for BOT env variable will run
  execute(message, args) {
    const client = new skynet.SkynetClient('https://siasky.net');

    if (args.length === 1) {
      return doCommand(client, message, args[0]);
    }
  },
};

const doCommand = async (client, message, arg) => {
  console.log('Getting info');

  const { contentType, metadata, skylink, url } = await getSkylinkInfo(
    client,
    arg
  );

  let title = metadata.filename;
  let icon = '';

  if (contentType === 'text/html' && 'index.html' in metadata.subfiles) {
    parsed = await getTitle(client, skylink);
    title = parsed.title;
    icon = parsed.icon ? parsed.icon : icon;
  }

  console.log('Building embed');

  const embed = buildEmbed(title, icon, contentType, metadata, skylink, url);

  return message.channel.send(embed);
};

const getSkylinkInfo = async (client, skylink) => {
  const metadata = await client.getMetadata(skylink);

  console.log('got metadata');

  const url = client.getSkylinkUrl(metadata.skylink);

  return { ...metadata, url };
};

const buildEmbed = (title, icon, contentType, metadata, skylink, url) => {
  const subfileCount = Object.keys(metadata.subfiles).length;

  const size = convertBytes(metadata.length);

  const embed = new Discord.MessageEmbed()
    .setColor('#58b560')
    .setTitle(`${title}`)
    .setURL(url)
    .setDescription(`${skylink}`)
    .setThumbnail(`${url}/${icon}`)
    .addField(contentType, 'content-type', true)
    .addField(size, 'size', true)
    .addField(subfileCount, 'subfiles', true)
    .setTimestamp()
    .setFooter(
      'Data from Siasky.net',
      'https://siasky.net/EACrYVcs3yJywxURYgsshsSMfqDaJxqOiCsqFdkbz98xUA'
    );

  return embed;
};

const getTitle = async (client, skylink) => {
  const url = client.getSkylinkUrl(skylink);
  const { data } = await client.getFileContent(skylink);
  let $ = await cheerio.load(data);

  let title = $('title').text();
  let icon = $('link[rel="icon"]')[0].attribs.href;

  let iconURL = new URL(icon, url);

  return { title, icon: iconURL.pathname };
};

//stolen from https://coderrocketfuel.com/article/how-to-convert-bytes-to-kb-mb-gb-or-tb-format-in-node-js

const convertBytes = function (bytes) {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

  if (bytes == 0) {
    return 'n/a';
  }

  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

  if (i == 0) {
    return bytes + ' ' + sizes[i];
  }

  return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};
