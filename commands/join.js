module.exports = {
  name: 'join',
  aliases: ['j'], // aliases for command name
  guildOnly: true, // can command be used in DMs?
  description: 'simulate new join to guild!',
  execute(message, args) {
    // This just simulates new user join for user making command.
    message.client.emit('guildMemberAdd', message.member);
  },
};
