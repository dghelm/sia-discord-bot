// This servers as the template for other commands.

module.exports = {
  name: 'args-info', // command name
  aliases: ['args', 'a'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: true, // return usage info if no args given
  guildOnly: false, // can command be used in DMs?
  devOnly: true, // if true only bot with 'dev' value for BOT env variable will run
  usage: '<arg1> <arg2> ...', //text helping provide usage info
  description: 'Information about the arguments provided.', // Help description
  execute(message, args) {
    message.channel.send(
      `Arguments: ${args}\nArguments length: ${args.length}\nGuild ID:${message.guild.id}`
    );
  },
};
