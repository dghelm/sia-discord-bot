// const { prefix } = require('../config.json');

const prefix = process.env.PREFIX;

const Welcome = require('../actions/welcome');

module.exports = {
  name: 'welcome', // command name
  aliases: ['w'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: false, // return usage info if no args given
  guildOnly: true, // can command be used in DMs?
  usage: '', //text helping provide usage info
  description: 'Asks new user poll. Command is for testing purposes.', // Help description
  execute(message, args) {
    return Welcome.execute(message);
  },
};
