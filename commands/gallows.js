// const { prefix } = require('../config.json');
const Gallows = require('../actions/gallows');
const prefix = process.env.PREFIX;

module.exports = {
  name: 'gallows',
  description: 'Sends user to the gallows!',
  aliases: ['g'],
  usage: '[username]',
  cooldown: 5,
  execute(message, args) {
    const data = [];
    const { commands } = message.client;

    //if only calling help without command

    if (!args.length) {
      data.push('I need a user!');

      return message.author.send(data, { split: true }).catch((error) => {
        console.error(
          `Could not send help DM to ${message.author.tag}.\n`,
          error
        );
        message.reply(
          "it seems like I can't DM you! Do you have DMs disabled?"
        );
      });
    }

    const taggedUser = message.mentions.users.first();

    return Gallows.sendTo(taggedUser, message.guild);
  },
};
