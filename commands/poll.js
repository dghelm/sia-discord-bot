// const { prefix } = require('../config.json');

const prefix = process.env.PREFIX;

const Poll = require('../actions/poll');

module.exports = {
  name: 'poll', // command name
  aliases: ['p'], // aliases for command name
  cooldown: 5, // amount of time in seconds user will have to wait before running again.
  args: false, // return usage info if no args given
  guildOnly: false, // can command be used in DMs?
  usage: '', //text helping provide usage info
  description: 'Asks new user poll. Command is for testing purposes.', // Help description
  execute(message, args) {
    return Poll.execute(message.author);
  },
};
